<div align="center">Etape pentru instalare</div> 

1. Se instaleaza ultima versiune de node.js: https://nodejs.org/en/
2. Se instaleaza Git Bash
3. Se deschide Git Bash in folderul dorit clik dreapta open with Git Bash si se dau comenzile:
        - npm init (se completeza liniile cerute daca este cazul)
        - npm install --save-dev sass
        - npm install bootstrap --save
        - npm install --save @fortawesome/fontawesome-free    (daca dorim fonturi)
        - npm install postcss-cli autoprefixer --save
4. Se deschide fisierul package.jason aparut in fisier dupa instalari si se sterge linia de script pentru
    a se completa cu  "scripts": {
    "compile:sass": "sass scss:original/css"
  },
4A - in folderul unde avem deschis Git Bash se creaza un folder scss cu un fisier style.scss in interior
        original/css = folderul in interiorul folderului in care se va salva css-ul
        Se da comanda in Git Bash: npm run compile:sass
5. Se modifica inca o data linia de script astfel:
     "scripts": {
    "compile:sass": "sass --watch scss:original/css"
  },
        Se da inca o data comanda: Se da comanda in Git Bash: <h3>npm run compile:sass</h3>
        Acum modificarile din scss se vor aplica in timp real in fisierul css
        Aceasta comanda se va da atunci cand lucram la proiect
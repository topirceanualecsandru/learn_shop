<div align="center">
<h1>Explicatii proiect</h1>
<h3>Etapele pentru creearea proiectului in ordine cronologica, punct cu punct</h3>
</div>

1. Deschidem pycharm: File/New Project si selecta Django - vom alege numele si locatia unde va fi salvat proiectul 
   precum si venv.
2. Instalam django din terminal: pip install django (in cazul in care nu este instalat)
3. Da comanda din terminal pentru a creea pproiectul: django-admin startproject "numele apliatiei"
   in cazul in care folosesti pycharm community
4. Pentru a crea prima aplicatie:python manage.py startapp "numeaplicatie"
5. Petru a porni si testa serverul: py manage.py runserver 
   Putem scrie si py manage.py runserver 8001 pentru a alege alt port in afara de 8001
   Pentru a inchide serverul din termina: CTRL + C
6. Pentru a crea un superuser: python manage.py createsuperuser 
   -verifica intai daca ai migrat catre servar toate modificarile: python manage.py migrate
7. Deschidem settings.py si la sectiunea INSTALLED_APPS adaugam numele aplicatiei initiate in genul:
   'numeaplicateie',: numeaplicatie.apps.NumeaplicatieCongig. (dar se poate trece si numele aplicatiei intre ghilimici)
8. In urls proiect cream path catre urls-ul aplicatiei: path('', include('start_blog.urls')),
   in aplicatie cream fisierul urls.py in care importam path din django si views din aplicatie
9. <h3>Test prima pagina</h3>  In views cream functia/Clasa pentru pagina home, in templates cream pagina Html
   in urls-ul aplicatie cream calea, ex:path('hello/', views.home, name='home'),

Comanda pentru a creea modele din terminal(python shell):  
python manage.py shell  --- de aici navigam prin directoare ca in exemplul:  
from products.models import Product  
Product.objects.all()



4. Pentru a creea modelele initiale in proiect vom intra pe models.py aici cream tabelele cu ajutorul claselor.

5. class Category(models.Model): se creaza tabelul Category cu ajutorul unei clase mostenind functionalitate din models.Model

6. modelul  slug cu Slagfield se foloseste asa: http://127.0.0.1:8000/slug :pentru a accesa

7. Dupa ce am creat modelele trebuie sa migram spre baza de date
   Se poate crea un script sau scrie comanda: py manage.py makemigrations.
   Trebuie sa avem modelul de user importat

8. Trebuie instalata libraria pip install Pillow (este un pachet ce se ocupa de imagini)

9. Cream un folder gol exemplu "media" unde vom pune pozele
   in setings.py la final vom adauga:  
   MEDIA_URL = '/media/' # spunem numele folderului creat  
   MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')  #ii aratam locatia

10. py manage.py runserver --- comanda pentru a rula serverul .

11. Pentru a face teste in consola scriem: py manage.py test     
se poate teste si cu pachetul coverage:
Pentru testare: # coverage run manage.py test
Pentru rezultate: # coverage report
12. 

